// Test trips endpoint to use ANY RANDOM trip
// Use schedules and predictions end points
// https://api-v3.mbta.com/docs/swagger/index.html
import fetch from "cross-fetch";
import { mtRandom, makeNMil } from "./utils";
import trainhorn from "../media/mbta_train_horn_2.mp3";
import { DateOptions, MbtaStop, Line, Answer, Rank } from "./classes";

(function () {
  const images = import.meta.glob("../../*/img/*", { as: "url", eager: true }),
    root = "https://api-v3.mbta.com",
    rootDoc = document.documentElement,
    correct = new Answer("correct", trainhorn, function (el) {
      return el.animate(
        [
          {
            stroke: "black",
          },
          {
            stroke: "lime",
          },
          {
            stroke: "black",
          },
        ],
        {
          duration: 500,
          iterations: Infinity,
        },
      );
    }),
    incorrect = new Answer(
      "incorrect",
      "https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3",
      function (el) {
        return el.animate(
          [
            {
              stroke: "black",
            },
            {
              stroke: "#da291c",
              offset: 0.15,
            },
            {
              stroke: "black",
              offset: 0.85,
            },
          ],
          {
            duration: 500,
            iterations: Infinity,
          },
        );
      },
    ),
    apikey = "1b8be318d66b41ba87c7e47dd32db7a4",
    stopEvent = "arrive", // arrive|depart
    answerRgx = /Incorrect|Correct/,
    rankNames = ["Gumba", "The Grinch", "Bumble Bee", "Super Mario"],
    rankObjects = [];

  let app,
    routeOptions = ["Red", "Green", "Orange", "Blue"],
    routePatterns = [],
    rtPattern = "Red-3-1", //Braintree - Alewife DEFAULT
    lines = [],
    answered = [],
    bgimages = [],
    bgimagesLast,
    datePicker,
    datePicked,
    stopsTotal,
    stops = [],
    stopIndex = 0,
    resetButton,
    backdrop,
    loader,
    lineSelected,
    modalMessage,
    red = 2,
    game = {
      active: false,
    };

  for (let image in images) {
    if (/background/i.test(images[image])) {
      bgimages.push(images[image]);
    }
  }

  bgimages.forEach((image) => {
    preload_image(image);
  });

  window.addEventListener("DOMContentLoaded", function () {
    app = document.getElementById("App");
    datePicker = document.getElementById("datePicker");
    backdrop = document.querySelectorAll(".backdrop")[0];
    loader = document.getElementById("isLoading");

    const dateOptions = new DateOptions();

    buildLineOptions();

    setTutorialHeight();

    this.addEventListener("resize", setTutorialHeight);

    // Set the min/max date range in the date picker
    dateOptions.setDatePickerAttributes().forEach((item) => {
      datePicker.setAttribute(item.attr, item.value);
    });

    datePicker.addEventListener("focus", function (e) {
      let type = e.target.attributes.type.value;

      if (type == "text") {
        e.target.setAttribute("type", "date");
      }
    });

    datePicker.addEventListener("focusout", function (e) {
      if (e.target.value === "" || e.target.value === undefined) {
        e.target.setAttribute("type", "text");
      }
    });

    datePicker.addEventListener("change", function (e) {
      stops = stops.length ? [] : stops;
      app.innerHTML = app.innerHTML.length ? "" : app.innerHTML;

      if (e.target.value !== "" || e.target.value !== undefined) {
        // MBTA swagger update no longer accepts YYYY-DD-MM and works only as YYYY-MM-DD
        let dateArray = e.target.value.split("-");
        //datePicked = dateArray[0] + "-" + dateArray[2] + "-" + dateArray[1];
        datePicked = e.target.value;
        lineSelected = document.querySelector(
          'input[name="line"]:checked',
        ).value;

        // https://stackoverflow.com/questions/8217419/how-to-determine-if-a-javascript-array-contains-an-object-with-an-attribute-that
        const i = lines.findIndex((e) => e.name === lineSelected);

        // Add class to body element
        if (i > -1) {
          let linesCanonical = lines[i].array.filter((line) => {
            if (line.attributes.canonical == true) {
              return line;
            }
          });

          /* vendors contains the element we're looking for, at index "i" */
          let randomIndex = getRandomInt(linesCanonical.length);

          rtPattern = linesCanonical[randomIndex].id;

          document.body.setAttribute("class", lineSelected.toLowerCase());
        }

        return playGame(datePicked);
      }
    });

    document.addEventListener("click", function (e) {
      e.stopPropagation();
      const resetButton = e.target.closest(".game-reset"),
        svgAnchor = e.target.closest(".clock > a"),
        gameStart = e.target.closest("#gameStart"),
        tutorialHelp = e.target.closest(".game-rules"),
        configsGameNonActive = document.querySelector(
          ".configs-game-nonactive",
        ),
        configsGameActive = document.querySelector(".configs-game-active"),
        gameTutorialInfo = document.querySelector(".tutorial");

      if (resetButton) {
        return reset(e);
      }

      if (svgAnchor) {
        checkIfCorrect(svgAnchor);
      }

      if (gameStart) {
        gameTutorialInfo.classList.toggle("expanded");

        if (!game.active) {
          configsGameNonActive.hidden = false;
          configsGameActive.hidden = true;
        } else {
          configsGameNonActive.hidden = true;
          configsGameActive.hidden = false;
        }
      }

      if (tutorialHelp) {
        gameTutorialInfo.classList.toggle("expanded");

        configsGameNonActive.hidden = true;
        configsGameActive.hidden = true;
      }
    });

    rankNames.map((name) => {
      let rank = new Rank(name);
      rankObjects.push(rank);
    });
  });

  async function playGame(date) {
    game.active = true;

    // If scheduled stops have already been generated...
    if (stops.length) {
      document.body.removeAttribute("style");

      renderChallenge(stops[stopIndex], stopEvent);

      return;
    }

    loader.style.display = "block";

    // Fetch request all trips from the specified date that matches the specified route pattern
    await fetch(
      `${root}/trips?filter[route_pattern]=${rtPattern}&filter[date]=${date}&api_key=${apikey}`,
    )
      .then((resp, error) => {
        if (!resp.ok) {
          return error;
        }

        return resp.json();
      })
      .then((data) => {
        let index = getRandomInt(data.data.length);

        // If trips exist, create an object from the FIRST trip data
        let trip = {
          id: data.data[index].id,
          headsign: data.data[index].attributes.headsign,
          direction: data.data[index].attributes.direction_id,
          valid: true,
        };

        return trip;
      })
      .then((trip) => {
        // Fetch request the FIRST trip schedule
        return fetch(
          `${root}/schedules?sort=stop_sequence&filter[trip]=${trip.id}&filter[date]=${date}&api_key=${apikey}`,
        )
          .then((resp, error) => {
            if (!resp.ok) {
              return error;
            }

            return resp.json();
          })
          .then((responseJson) => {
            return createStops(responseJson, trip.headsign);
          })
          .then((respStops) => {
            loader.style.display = "none";

            document.querySelector(".configs-game-nonactive").hidden = true;
            document.querySelector(".configs-game-active").hidden = false;

            return renderChallenge(respStops[stopIndex], stopEvent);
          })
          .catch((error) => {
            console.log(error);

            loader.style.display = "none";

            app.innerHTML =
              '<p class="text error">Something is wrong with the schedule on this date :( <br>Try again :)</p>';
          });
      })
      .catch((error) => {
        console.log(error);

        loader.style.display = "none";

        app.innerHTML =
          '<p class="text error">Something is wrong with the trips on this date :( <br>Try again :)</p>';
      });
  }

  function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  function getStopData(data) {
    return fetch(
      `${root}/stops/${data.relationships.stop.data.id}?api_key=${apikey}`,
    )
      .then((resp, error) => {
        if (!resp.ok) {
          return error;
        }

        return resp.json();
      })
      .then((responseJson) => {
        return responseJson.data;
      })
      .catch(() => {});
  }

  async function buildLineOptions() {
    const routes = await fetch("https://api-v3.mbta.com/route_patterns")
      .then((resp, error) => {
        if (!resp.ok) {
          return error;
        }

        return resp.json();
      })
      .then((data) => {
        for (let i = 0; i < routeOptions.length; i++) {
          let line = new Line(routeOptions[i]);
          lines.push(line);
        }

        return data;
      })
      .then((data) => {
        let filterRoutePatterns = [];

        lines.forEach((line) => {
          filterRoutePatterns = data.data.filter((item) => {
            if (item.relationships.route.data.id.match(`^${line.name}`)) {
              return item.relationships.route.data.id;
            }
          });
          line.array = filterRoutePatterns;
        });

        return lines;
      })
      .then((lines) => {
        let contentContainer = document.querySelector(".line-option>.col"),
          linesContainer = "";

        lines.map((line, index) => {
          if (index == 0) {
            return (linesContainer += `<label><input checked id="option_${index}" type="radio" name="line" value="${line.name}" />${line.name}</label>`);
          }
          return (linesContainer += `<label><input id="option_${index}" type="radio" name="line" value="${line.name}" />${line.name}</label>`);
        });

        return (contentContainer.innerHTML = `<form><legend><h2>Choose the train line</h2></legend><fieldset>${linesContainer}</fieldset></form>`);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function createStops(schedule, headsign) {
    for (const data of schedule.data) {
      // Use data retrieved from the schedules api to access data from the stops api
      await getStopData(data)
        .then((data) => {
          return data;
        })
        .then((stopData) => {
          let stopObj = new MbtaStop(data);
          stopObj.sign = headsign;
          stopObj.name = stopData.attributes.name;
          stopObj.direction = stopData.attributes.direction_id;
          stopObj.parentStation = stopData.relationships.parent_station.data.id;

          stopObj.bg = function () {
            let regex = new RegExp(`${this.parentStation}`, "i"),
              url = Object.values(bgimages).filter((image) => {
                const imgNameMatches = regex.test(image);
                if (imgNameMatches) {
                  return image;
                }
              });
            return url[0];
          };

          return stops.push(stopObj);
        })
        .catch(() => {});
    }

    //resetButton.removeAttribute('hidden');

    stopsTotal = stops.length;

    return stops;
  }

  function preload_image(imgUrl) {
    const img = document.createElement("img");
    img.src = imgUrl;
    img.style.display = "none";
    img
      .decode()
      .then(() => {
        document.body.appendChild(img);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function renderChallenge(stop, stopEvent) {
    let direction = stop.direction_id == 0 ? "Southbound" : "Northbound",
      analogClock = regTimeCorrect(stop),
      ing = stopEvent == "depart" ? "leaving" : "arriving";

    // If the game clock does not already have an hour, minute and hour/minute array ...
    if (analogClock.timeArray != true) {
      analogClock["svg_" + stopEvent] = regTimeSvg(analogClock);

      analogClock.svg_incorrect = regTimeSvg(regTimeIncorrect(stop));

      const hourNumbers = `<span class="analog number twelve">12</span>
          <span class="analog number one">1</span>
          <span class="analog number two">2</span>
          <span class="analog number three">3</span>
          <span class="analog number four">4</span>
          <span class="analog number five">5</span>
          <span class="analog number six">6</span>
          <span class="analog number seven">7</span>
          <span class="analog number eight">8</span>
          <span class="analog number nine">9</span>
          <span class="analog number ten">10</span>
          <span class="analog number eleven">11</span>`;

      let correct = `<div class="col col-12 col-sm-12 col-lg-6 col-xl-6 clock">${
          hourNumbers + analogClock["svg_" + stopEvent]
        }</div>`,
        incorrect = `<div class="col col-12 col-sm-12 col-lg-6 col-xl-6 clock">${
          hourNumbers + analogClock.svg_incorrect
        }</div>`,
        svgObj = [correct, incorrect],
        left = analogClock.position,
        right = left == 0 ? 1 : 0;

      return (app.innerHTML = `<div class="row stop-data">
        <h2 class="col-12 col-sm-6 title">
        <span class="text white t-message semi-bold">
        ${analogClock.name} /
        <span class="text light">
        ${direction}
        &nbsp;to&nbsp;
        ${stop.sign}
        </span>
        </span>
        </h2>
        <div class="col-12 col-sm-6 challenge">
        <p class="text yellow t-message bold">The <span class="text icon white"><span class="abbr">${lineSelected.substring(
          0,
          1,
        )}L</span></span> train is ${ing} @ ${analogClock.timeArray[2]}</p>
        </div></div><div class="row score-data">
        <div class="col-12 status"></div></div>
        <div class="row">${svgObj[left]}${svgObj[right]}</div>`);
    }
  }

  function regTimeCorrect(stop) {
    let timeArray = stop.arrive ? makeNMil(stop.arrive) : makeNMil(stop.depart),
      obj = {
        timeArray: timeArray == false ? false : timeArray,
        id: stop.id,
        position: Math.floor(Math.random(1, 2) * 2),
        name: stop.name,
        correct: true,
      };

    return obj;
  }

  function regTimeIncorrect(stop) {
    let obj = {
      timeArray: makeNMil(mtRandom()),
      id: stop.id,
      correct: false,
    };

    return obj;
  }

  function regTimeSvg(s) {
    let id,
      hour = s.timeArray[0],
      min = s.timeArray[1];

    if (s.correct === false) {
      id = "clockIncorrect_stopId-" + s.id;
    } else {
      id = "clockCorrect_stopId-" + s.id;
    }

    if (s != "") {
      let html = "",
        minuteHandle = Number(min) * 6,
        hourHandle = Number(hour) * 30 + min / 2;
      //https://cssanimation.rocks/clocks/

      html = `<a id="${id}">
        <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events" class="analog" xmlns="http://www.w3.org/2000/svg"  viewbox="0 0 40 40">
        <defs><title></title><style></style></defs>
        <circle cx="20" cy="20" r="19" class="face" />
        <g class="marks">
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        <line x1="15" y1="0" x2="16" y2="0" />
        </g>
        <line x1="0" y1="0" x2="9" y2="0" class="hour" transform="translate(20,20) rotate(${hourHandle})" />
        <line x1="0" y1="0" x2="13" y2="0" class="minute" transform="translate(20,20) rotate(${minuteHandle})" /><circle cx="20" cy="20" r="0.7" class="pin" />
        </svg>
        </a>`;

      return html;
    }
  }

  async function checkIfCorrect(anchor) {
    let id = anchor.getAttribute("id"),
      answerString = answerRgx.exec(id),
      clockSvg = document.querySelector("#" + id + " svg"),
      challenge = document.querySelector(".challenge"),
      challengePrevious = challenge.innerHTML;

    document.body.style.pointerEvents = "none";

    if (answerString == "Correct") {
      backdrop.style.display = "block";

      correct.audioSrc.play();
      correct.animation(clockSvg);

      challenge.innerHTML =
        '<span class="text yellow t-message bold">Good job!</span>';

      correct.audioSrc.addEventListener(
        "ended",
        async () => {
          clockSvg.getAnimations().map((anim) => anim.cancel());
          stopIndex++;

          document.body.removeAttribute("style");
          backdrop.style.display = "none";

          if (stopIndex < stops.length - 1) {
            return await playGame(datePicked);
          } else {
            rootDoc.style.setProperty(
              "--bg-img_splash",
              `url('${stops[stopIndex].bg()}')`,
            );
            app.innerHTML = `<p class="text congratulations">Congratulations you made it to ${stops[stopIndex].name} ON TIME!!!</p><br><button class="button congratulations game-reset" type="reset">Try again!</button>`;
          }
        },
        { once: true },
      );

      correct.audioSrc.addEventListener(
        "ended",
        async () => {
          if (answerString == "Incorrect") {
            return postAnswer(id.slice(-5), "incorrect");
          } else {
            return postAnswer(id.slice(-5), "correct");
          }
        },
        { once: true },
      );
    } else {
      //answerObj = incorrect;

      incorrect.audioSrc.play();
      incorrect.animation(clockSvg);

      challenge.innerHTML =
        '<span class="text yellow t-message bold">Keep trying!</span>';

      incorrect.audioSrc.addEventListener("ended", async () => {
        challenge.innerHTML = challengePrevious;

        clockSvg.getAnimations().map((anim) => anim.cancel());
        document.body.removeAttribute("style");
      });

      incorrect.audioSrc.addEventListener(
        "ended",
        async () => {
          if (answerString == "Incorrect") {
            return postAnswer(id.slice(-5), "incorrect");
          } else {
            return postAnswer(id.slice(-5), "correct");
          }
        },
        { once: true },
      );
    }
  }

  function postAnswer(id, result) {
    let obj = {};

    obj.id = id;
    obj.result = result;
    obj.submitted = true;

    const found = answered.some((el) => el.id === id);

    if (!found) answered.push(obj);

    if (stopIndex < stops.length - 1) {
      return renderProgress();
    }
  }

  function renderProgress() {
    let progress = document.querySelectorAll(".status")[0],
      correct = [],
      incorrect = [];

    for (let i = 0; i < answered.length; i++) {
      switch (answered[i].result) {
        case "incorrect":
          incorrect.push(answered[i]);
          break;
        case "correct":
          correct.push(answered[i]);
          break;
      }
    }

    progress.innerHTML = `<span class="t-message"><strong class="correct"><span class="progress-background"></span><span class="count">${
      correct.length
    }</span> <span class="label">Right</span></strong><strong class="incorrect"><span class="progress-background"></span><span class="count">${
      incorrect.length
    }</span> <span class="label">Wrong</span></strong><strong class="total"><span class="progress-background"></span><span class="count">${Number(
      stops.length - answered.length,
    )}</span> <span class="label">Stops left</span></strong></span>`;

    calculateRank(correct.length, incorrect.length, stops.length);
  }

  function reset() {
    game.active = false;

    document.querySelector(".configs-game-nonactive").hidden = false;
    document.querySelector(".configs-game-active").hidden = true;

    datePicker.setAttribute("type", "text");
    datePicker.value = "";
    datePicker.setAttribute("placeholder", "Pick your date");
    app.innerHTML = "";
    stopIndex = 0;
    stops = [];
    answered = [];
  }

  function setTutorialHeight() {
    const header = document.querySelector(".row-fluid.tutorial > .col");
    let vh = header.clientHeight;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
  }

  function calculateRank(totalCorrect, totalIncorrect, totalStops) {
    red += 10;
    let bgCorrect = document.querySelector(".correct .progress-background"),
      bgIncorrect = document.querySelector(".incorrect .progress-background"),
      bgTotal = document.querySelector(".total .progress-background"),
      percentCorrect = Math.round((totalCorrect / totalStops) * 100),
      percentIncorrect = Math.round((totalIncorrect / totalStops) * 100),
      percentOfTotal = Math.round(
        ((totalCorrect + totalIncorrect) / totalStops) * 100,
      ),
      //rankMessage = "",
      speed =
        10 - (totalCorrect / totalStops) * 10 > 1
          ? 10 - (totalCorrect / totalStops) * 10
          : 1,
      backgroundImageWebkit = `-webkit-linear-gradient(90deg, rgba(${red}, 0, 36, 1) 0, rgba(9, 9, 121, 1) 25%, rgba(0, 212, 255, 1) 50%, rgba(9, 9, 121, 1) 75%, rgba(${red}, 0, 36, 1) 100%)`,
      backgroundImageMoz = `-moz-linear-gradient(90deg, rgba(${red}, 0, 36, 1) 0, rgba(9, 9, 121, 1) 25%, rgba(0, 212, 255, 1) 50%, rgba(9, 9, 121, 1) 75%, rgba(${red}, 0, 36, 1) 100%)`,
      backgroundImageO = `-o-linear-gradient(90deg, rgba(${red}, 0, 36, 1) 0, rgba(9, 9, 121, 1) 25%, rgba(0, 212, 255, 1) 50%, rgba(9, 9, 121, 1) 75%, rgba(${red}, 0, 36, 1) 100%)`,
      backgroundImage = `linear-gradient(90deg, rgba(${red}, 0, 36, 1) 0, rgba(9, 9, 121, 1) 25%, rgba(0, 212, 255, 1) 50%, rgba(9, 9, 121, 1) 75%, rgba(${red}, 0, 36, 1) 100%)`;

    bgCorrect.style.setProperty("--bg-width", percentCorrect + "%");
    bgIncorrect.style.setProperty("--bg-width", percentIncorrect + "%");
    bgTotal.style.setProperty("--bg-width", percentOfTotal + "%");

    if (percentCorrect >= 50) {
      bgCorrect.style.setProperty("--bg-animate", speed + "s");

      bgCorrect.style.setProperty(
        "--bg-gradient-webkit",
        backgroundImageWebkit,
      );
      bgCorrect.style.setProperty("--bg-gradient-moz", backgroundImageMoz);
      bgCorrect.style.setProperty("--bg-gradient-o", backgroundImageO);
      bgCorrect.style.setProperty("--bg-gradient", backgroundImage);
    }
  }
})();

//https://stackoverflow.com/questions/23367345/100vw-causing-horizontal-overflow-but-only-if-more-than-one

//https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API

//https://www.scaler.com/topics/javascript-namespace/
