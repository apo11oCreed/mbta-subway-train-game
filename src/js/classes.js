export class DateOptions {
    constructor() {
        this.dateCurrent = new Date();
        this.range = { min: "", max: "" };
        this.dateMin = new Date();
        this.dateMax = new Date();
        this.year = this.dateCurrent.getFullYear().toString();
        this.month =
            Number(this.dateCurrent.getMonth()) + 1 < 10
                ? Number(this.dateCurrent.getMonth() + 1)
                    .toString()
                    .padStart(2, "0")
                : Number(this.dateCurrent.getMonth() + 1).toString();
    }
    setRange() {
        this.dateMin.setDate(this.dateCurrent.getDate() - 1);
        this.dateMax.setDate(this.dateCurrent.getDate() + 1);
        this.range.min = `${this.year}-${this.dateMin.getMonth() + 1}-${this.setDayFormat(this.dateMin)}`;
        this.range.max = `${this.year}-${this.dateMax.getMonth() + 1}-${this.setDayFormat(this.dateMax)}`;
        return this.range;
    }
    setDayFormat(option) {
        return Number(option.getDate()) < 10
            ? Number(option.getDate()).toString().padStart(2, "0")
            : Number(option.getDate()).toString();
    }
    setDatePickerAttributes() {
        this.setRange();
        return [
            {
                attr: "min",
                value: this.range.min,
            },
            {
                attr: "max",
                value: this.range.max,
            },
        ];
    }
}
export class MbtaStop {
    constructor(d) {
        this.arrive = d.attributes.arrival_time;
        this.depart = d.attributes.departure_time;
        this.sign = d.attributes.headsign;
        this.sequence = d.attributes.stop_sequence;
        this.id = d.relationships.stop.data.id;
        this.direction_id = d.attributes.direction_id;
    }
}
export class Line {
    constructor(name, array) {
        this.name = name;
        this.array = array;
    }
}
export class Answer {
    constructor(status, audioSrc, animationObj) {
        this.status = status;
        this.audioSrc = new Audio(audioSrc);
        this.animation = animationObj;
    }
}
export class Rank {
    constructor(name, options) {
        this.name = name;
        this.options = options;
    }
}
