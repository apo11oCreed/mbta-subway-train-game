type dateString = {
  getDate: () => number;
};

interface DateProps {
  dateCurrent: Date;
  range: { min: string; max: string };
  dateMin: Date;
  dateMax: Date;
  year: string;
  month: string;
}

interface Stop {
  arrive: string;
  depart: string;
  sign: string;
  sequence: string;
  id: string;
  direction_id: string;
  d: object;
}

interface LineData {
  name: string;
  array: string[];
}

interface Result {
  status: string;
  audioSrc: HTMLAudioElement;
  animation: Animation;
}

export class DateOptions implements DateProps {
  dateCurrent: Date;
  range: { min: string; max: string };
  dateMin: Date;
  dateMax: Date;
  year: string;
  month: string;
  constructor() {
    this.dateCurrent = new Date();
    this.range = { min: "", max: "" };
    this.dateMin = new Date();
    this.dateMax = new Date();
    this.year = this.dateCurrent.getFullYear().toString();
    this.month =
      Number(this.dateCurrent.getMonth()) + 1 < 10
        ? Number(this.dateCurrent.getMonth() + 1)
            .toString()
            .padStart(2, "0")
        : Number(this.dateCurrent.getMonth() + 1).toString();
  }
  setRange() {
    this.dateMin.setDate(this.dateCurrent.getDate() - 1);
    this.dateMax.setDate(this.dateCurrent.getDate() + 1);
    this.range.min = `${this.year}-${
      this.dateMin.getMonth() + 1
    }-${this.setDayFormat(this.dateMin)}`;
    this.range.max = `${this.year}-${
      this.dateMax.getMonth() + 1
    }-${this.setDayFormat(this.dateMax)}`;
    return this.range;
  }
  setDayFormat(option: dateString) {
    return Number(option.getDate()) < 10
      ? Number(option.getDate()).toString().padStart(2, "0")
      : Number(option.getDate()).toString();
  }
  setDatePickerAttributes() {
    this.setRange();
    return [
      {
        attr: "min",
        value: this.range.min,
      },
      {
        attr: "max",
        value: this.range.max,
      },
    ];
  }
}

export class MbtaStop implements Stop {
  arrive: string;
  depart: string;
  sign: string;
  sequence: string;
  id: string;
  direction_id: string;
  d: object;
  constructor(d: {
    attributes: {
      arrival_time: string;
      departure_time: string;
      headsign: string;
      stop_sequence: string;
      direction_id: string;
    };
    relationships: { stop: { data: { id: string } } };
  }) {
    this.arrive = d.attributes.arrival_time;
    this.depart = d.attributes.departure_time;
    this.sign = d.attributes.headsign;
    this.sequence = d.attributes.stop_sequence;
    this.id = d.relationships.stop.data.id;
    this.direction_id = d.attributes.direction_id;
  }
}

export class Line implements LineData {
  name: string;
  array: string[];
  constructor(name: string, array: string[]) {
    this.name = name;
    this.array = array;
  }
}

export class Answer implements Result {
  status: string;
  audioSrc: HTMLAudioElement;
  animation: Animation;
  constructor(status: string, audioSrc: string, animationObj: Animation) {
    this.status = status;
    this.audioSrc = new Audio(audioSrc);
    this.animation = animationObj;
  }
}

export class Rank {
  name: string;
  options: object;
  constructor(name: string, options: object) {
    this.name = name;
    this.options = options;
  }
}
