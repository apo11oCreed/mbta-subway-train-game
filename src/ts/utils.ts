function militaryTimeRandomize() {
  let hour: number = Math.floor(Math.random() * (24 - 0) + 0),
    minute: number = Math.floor(Math.random() * (60 - 0) + 0),
    hourString: string = hour < 10 ? "0" + hour.toString() : hour.toString(),
    minuteString: string =
      minute < 10 ? "0" + minute.toString() : minute.toString();

  return `${hourString}:${minuteString}:00`;
}

function makeNonMilitary(time: string) {
  if (time == null || typeof time === undefined) {
    return false;
  }

  let militaryTimeRgx: RegExp = new RegExp(
      /[0-9]{2,2}:[0-9]{2,2}:[0-9]{2,2}/,
      "g",
    ),
    militaryTime: RegExpExecArray = militaryTimeRgx.exec(time),
    calculateHour: string =
      parseInt(militaryTime[0], 10) > 12
        ? (parseInt(militaryTime[0], 10) - 12).toString()
        : parseInt(militaryTime[0], 10).toString(),
    hour: string = calculateHour == "0" ? "12" : calculateHour,
    minute: string = militaryTime[0].substr(3, 2),
    amPm: string = parseInt(militaryTime[0], 10) > 12 ? "PM" : "AM",
    timeArray = [];

  timeArray.push(hour);
  timeArray.push(minute);
  timeArray.push(parseInt(hour, 10) + ":" + minute + amPm);

  return timeArray;
}

export { militaryTimeRandomize as mtRandom, makeNonMilitary as makeNMil };
