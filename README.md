# MBTA Subway Train Game

This is a game intended to help teach kids how to read time on analog clocks. Challenges are based on the **MBTA** scheduling and train routes.

## Configs

To sort order CSS: `npx postcss --no-map --replace ./src/css/styles.css`

## Enhancements
- [x] Ensure route directions are accurate. Ex. Forest Hills to Oak Grove is not Northbound
- [x] Add ranks based on percentage correct vs incorrect. Ex. Rookie,
- [ ] Optimize CSS/JS modules
- [x] motivational messaging as response to correct and incorrect attempts throught gameplay
- [x] Provide scorecard that displays correct vs incorrect attempts throughout gameplay
- [x] Add station photos to each stop
- [x] Provide option to select a different route pattern
- [x] Refine Reset UX. Datepicker and line picker are not relevant during a game but required to start a game.
- [x] Ensure the reset discards all previous records to start a new game
- [x] Color code route pattern experience to match train line
- [x] Add a11y checking. Implemented pa11y but the process will not run in AWS Cloud9 due to 8mb file size limit
- [x] Add Cypress to test E2E
- [x] Provide option to view rules or 'Play Game!' throughout the visitor session