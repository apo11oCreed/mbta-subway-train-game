// vite.config.js
import cleanPlugin from 'vite-plugin-clean';

export default {
    // config options
    base: '/mbta-subway-train-game/',
    publicDir: 'public',
    server: {
        port: 8080
    },
    preview: {
        port: 8080,
    },
    plugins: [cleanPlugin({
        targetFiles: ['dist']
      })],
    build: {
        watch: false,
        outDir: 'dist',
        cssCodeSplit: true,
        sourcemap: 'inline',
        terser: true,
        TerserOptions:{
            mangle: false
        },
        rollupOptions: {
			output: {
				assetFileNames: (assetInfo) => {
					let extType = assetInfo.name.split('.').at(1);
					if (/png|jpe?g|svg|gif|tiff|bmp|ico/i.test(extType)) {
						extType = 'img';
					}
					return `${extType}/[name]-[hash][extname]`;
				},
				chunkFileNames: 'js/[name]-[hash].js',
				entryFileNames: 'js/[name]-[hash].js',
			},
		},
      }
  }