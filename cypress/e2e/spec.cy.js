describe('My First Test', () => {
  it('clicking on calendar', () => {
    cy.visit('https://apo11ocreed.gitlab.io/mbta-subway-train-game/')

    cy.contains('input').click()

    // Should be on a new URL which
    // includes '/commands/actions'

    cy.get('#datepicker')
  })
})